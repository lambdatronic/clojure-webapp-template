DROP DATABASE IF EXISTS my_project;
DROP ROLE IF EXISTS my_project;
CREATE ROLE my_project WITH LOGIN CREATEDB PASSWORD 'my_project';
CREATE DATABASE my_project WITH OWNER my_project;
\c my_project
CREATE EXTENSION pgcrypto;

(ns my-project.pages.home
  (:require [goog.dom :as dom]
            [reagent.core :as r]))

(defn root-component []
  [:div
   [:h1 "This is the default home page. Put something here."]])

(defn ^:export init [_]
  (r/render root-component (dom/getElement "app")))

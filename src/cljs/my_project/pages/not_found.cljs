(ns my-project.pages.not-found
  (:require [goog.dom :as dom]
            [reagent.core :as r]
            [my-project.utils :as u]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UI Components
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn root-component []
  [:div
   [:h1 "Page Not Found"]
   [:input {:type "button"
            :value "Go To Landing Page"
            :on-click #(u/jump-to-url! "/")}]])

(defn ^:export init [_]
  (r/render root-component (dom/getElement "app")))

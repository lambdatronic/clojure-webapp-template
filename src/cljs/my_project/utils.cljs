(ns my-project.utils
  (:require [cljs.reader :as edn]
            [clojure.string :as str]
            [clojure.core.async :refer [chan go >! <! close!]]))

;;; Getting input field values

(defn input-value
  "Return the value property of the target property of an event."
  [event]
  (-> event .-target .-value))

(defn input-int-value
  [event]
  (js/parseInt (input-value event)))

(defn input-float-value
  [event]
  (js/parseFloat (input-value event)))

(defn input-file
  "Return the file of the target property of an event."
  [event]
  (-> event .-target .-files (aget 0)))

;;; Storing/Retrieving data in the browser's session storage

(def session-key "my-project")

(defn- save-session-storage! [data]
  (.setItem (.-sessionStorage js/window) session-key (pr-str data)))

(defn get-session-storage []
  (edn/read-string (.getItem (.-sessionStorage js/window) session-key)))

(defn set-session-storage! [data]
  (save-session-storage! (merge (get-session-storage) data)))

(defn remove-session-storage! [& keys]
  (let [data (get-session-storage)]
    (save-session-storage! (apply dissoc data keys))))

(defn clear-session-storage! []
  (save-session-storage! {}))

;;; Page navigation

(defn jump-to-url!
  ([url]
   (let [origin (.-origin (.-location js/window))
         cur-url (str/replace (.-href (.-location js/window)) origin "")]
     (when-not (= cur-url url) (set! (.-location js/window) url))))
  ([url window-name]
   (if window-name
     (.open js/window url window-name)
     (jump-to-url! url))))

;;; Asynchronous server-side calls

(defmulti call-remote! (fn [method url data] method))

;; FIXME: Come up with your own auth-token and set it in handler.clj's token-resp function.
(defmethod call-remote! :get [_ url data]
  (let [query-string (->> data
                          (map (fn [[k v]] (str (pr-str k) "=" (pr-str v))))
                          (str/join "&")
                          (js/encodeURIComponent))
        fetch-params {:method "get"
                      :headers {"Accept" "application/edn"
                                "Content-Type" "application/edn"}}
        result-chan  (chan)]
    (-> (.fetch js/window
                (str url
                     "?auth-token=KJlkjhasduewlkjdyask-dsf"
                     (when (= query-string "") (str "&" query-string)))
                (clj->js fetch-params))
        (.then  (fn [response]   (if (.-ok response) (.text response) (.reject js/Promise response))))
        (.then  (fn [edn-string] (go (>! result-chan (or (edn/read-string edn-string) :no-data)))))
        (.catch (fn [response]   (.log js/console response) (close! result-chan))))
    result-chan))

;; FIXME: Come up with your own auth-token and set it in handler.clj's token-resp function.
(defmethod call-remote! :post [_ url data]
  (let [fetch-params {:method "post"
                      :headers (merge {"Accept" "application/edn"}
                                      (when-not (= (type data) js/FormData)
                                        {"Content-Type" "application/edn"}))
                      :body (cond
                              (= js/FormData (type data)) data
                              data                        (pr-str data)
                              :else                       nil)}
        result-chan  (chan)]
    (-> (.fetch js/window
                (str url "?auth-token=KJlkjhasduewlkjdyask-dsf")
                (clj->js fetch-params))
        (.then  (fn [response]   (if (.-ok response) (.text response) (.reject js/Promise response))))
        (.then  (fn [edn-string] (go (>! result-chan (or (edn/read-string edn-string) :no-data)))))
        (.catch (fn [response]   (.log js/console response) (close! result-chan))))
    result-chan))

(defmethod call-remote! :default [method _ _]
  (throw (str "No such method (" method ") defined for my-project.utils/call-remote!")))

(defn call-clj-async! [clj-fn-name & args]
  (call-remote! :post
                (str "/clj/" clj-fn-name)
                {:clj-args args}))

(defn call-sql-async! [sql-fn-name & args]
  (let [[schema function] (str/split sql-fn-name #"\.")]
    (call-remote! :post
                  (str "/sql/" schema "/" function)
                  {:sql-args args})))
